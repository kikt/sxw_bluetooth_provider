# sxw_bluetooth_provider

与食迅网业务逻辑相关的 Bluetooth 管理库

使用 ChangeNotifier 作为核心状态管理

## 进度

- [x] ble
  - [x] 连接
  - [x] 断开
  - [x] 接受
  - [x] 发送
- [x] spp

  - [x] 连接
  - [x] 断开
  - [x] 接受
  - [x] 发送

- [x] 连接模板

  - [x] 打印机
    - [x] ble
    - [x] spp
  - [x] 电子秤
    - [x] ble
    - [x] spp

- 其他
  - [ ] 监听 spp 和 ble 的设备连接状态并发出通知, 如有必要则修改 ble/spp 底层
