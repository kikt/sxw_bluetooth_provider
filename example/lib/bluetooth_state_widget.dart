import 'package:flutter/material.dart';
import 'package:sxw_bluetooth_provider/sxw_bluetooth_provider.dart';

class BluetoothStateWidget extends StatelessWidget {
  final SxwBluetoothProvider provider;

  BluetoothStateWidget(this.provider);

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: provider,
      builder: (_, __) {
        final names =
            provider.devices.map((v) => "${v.name}${v.deviceType}").join(",");
        print("names = $names");
        return Container(
          alignment: Alignment.center,
          child: Text(names),
        );
      },
    );
  }
}
