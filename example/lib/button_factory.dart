import 'package:flutter/material.dart';

Widget createTextButton(String text, Function onPressed) {
  return RaisedButton(
    onPressed: onPressed,
    child: Text(text),
  );
}

Widget createIconButton(IconData data, Function onPressed) {
  return IconButton(icon: Icon(data), onPressed: onPressed);
}
