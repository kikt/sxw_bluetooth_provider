import 'package:oktoast/oktoast.dart';
import 'package:sxw_bluetooth_provider/sxw_bluetooth_provider.dart';

class ToastErrorHandler extends ErrorHandler {
  @override
  void onError(dynamic exception) {
    if (exception is BluetoothError) {
      showToast(exception.message);
    } else if (exception is Error) {
      showToast(exception.stackTrace.toString());
    } else if (exception is Exception) {
      showToast(exception.toString());
    } else {
      showToast(exception.message);
    }
  }
}
