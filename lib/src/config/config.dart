import 'dart:typed_data';

import 'package:flutter/cupertino.dart';
import 'package:sxw_bluetooth_provider/src/device/bluetooth_device.dart';
import 'package:sxw_bluetooth_provider/src/device/platform_enum.dart';
import 'package:sxw_bluetooth_provider/src/provider.dart';

abstract class BluetoothConnectConfig<W> {
  BtType get btType;

  String get name;

  String get platformType;

  Duration scanTimeout;

  BtTransformer<W, Uint8List> writeTransformer;

  PlatformEnum get platformTypeEnum;
}

abstract class BleConfig<W> extends BluetoothConnectConfig<W> {
  String notifyServiceId;

  String notifyChId;

  String writeServiceId;

  String writeChId;

  @override
  BtType get btType => BtType.ble;

  @override
  String name;

  BleConfig({
    this.notifyServiceId,
    this.notifyChId,
    this.writeServiceId,
    this.writeChId,
    this.name,
    Duration duration,
  }) {
    scanTimeout = duration;
    scanTimeout ??= const Duration(seconds: 3);
  }
}

abstract class SppConfig<W, R> extends BluetoothConnectConfig<W> {
  @override
  String name;

  String pin;

  @override
  BtType get btType => BtType.spp;

  SppConfig({
    @required this.name,
    @required this.pin,
    Duration duration,
  }) {
    scanTimeout = duration;
    scanTimeout ??= const Duration(seconds: 60);
  }
}
