import 'dart:async';

import 'package:bluetooth_ble/bluetooth_ble.dart';
import 'package:sxw_bluetooth_provider/src/config/config.dart';

import '../device/bluetooth_device.dart';

abstract class ConnectionHelper<T extends SxwBluetoothDevice,
    C extends BluetoothConnectConfig> {
  void connect(C config, Completer<SxwBluetoothDevice> completer);

  List<T> get connectedDeviceList;
}

abstract class ConnectionDelegate {
  void onConnectedDeviceChange();

  void onNotifyNotFoundCh(BleDevice device);
}
