part of 'bluetooth_device.dart';

class SxwBleDevice<WriteData, RecData>
    extends SxwBluetoothDevice<BleDevice, RecData, WriteData> {
  BleConfig<WriteData> config;

  SxwBleDevice({
    String debugLabel = "",
    @required BleDevice bleDevice,
    @required this.config,
  }) : super(debugLabel, bleDevice);

  @override
  String get id => device.id;

  @override
  String get name => device.name;

  @override
  void disconnect() {
    device.disconnect();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Future<void> _realWrite(Uint8List data) async {
    final ch = await device.findCh(config.writeServiceId, config.writeChId);
    if (ch == null) {
      return;
    }
    await device.writeData(ch: ch, data: data);
  }
}
