part of 'bluetooth_device.dart';

class SxwSppDevice<RecData, WriteData>
    extends SxwBluetoothDevice<BluetoothSppDevice, RecData, WriteData> {
  SxwSppDevice({
    String debugLabel = "",
    @required BluetoothSppDevice device,
    @required this.config,
  }) : super(debugLabel, device);

  @override
  String get name => device.name;

  @override
  String get id => device.mac;

  @override
  SppConfig<WriteData, RecData> config;

  @override
  void disconnect() async {
    await device.refreshBluetoothConnectionState();
    if (device.connection.isConnected) {
      device.connection.disconnect();
    }
  }

  @override
  Future<void> _realWrite(Uint8List data) async {
    await device.refreshBluetoothConnectionState();
    await device.connection.sendData(data);
  }
}
