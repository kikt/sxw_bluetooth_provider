mixin BluetoothError on Exception {
  String get message;
}

class NotSupportPlatformException implements BluetoothError, Exception {
  const NotSupportPlatformException();

  String get message => "未支持的设备类型";
}

class SwitchNoOpenException implements BluetoothError, Exception {
  const SwitchNoOpenException();

  @override
  String get message => "蓝牙开关未开启";
}

class DeviceNotFoundException implements BluetoothError, Exception {
  const DeviceNotFoundException();

  @override
  String get message => "没找到设备";
}

class ServiceNotFoundException implements BluetoothError, Exception {
  const ServiceNotFoundException();

  @override
  String get message => "没找到服务";
}

class CharacteristicNotFoundException implements BluetoothError, Exception {
  const CharacteristicNotFoundException();

  @override
  String get message => "特征码未找到";
}

class SppConnectException implements BluetoothError, Exception {
  const SppConnectException();

  @override
  String get message => "蓝牙连接失败";
}

class BtErrors {
  BtErrors._();

  static const notSupportPlatform = NotSupportPlatformException();
  static const switchNoOpen = SwitchNoOpenException();
  static const deviceNotFound = DeviceNotFoundException();
  static const serviceNotFound = ServiceNotFoundException();
  static const characteristicNotFound = CharacteristicNotFoundException();

  static const connectError = SppConnectException();
}

abstract class ErrorHandler {
  ErrorHandler();

  factory ErrorHandler.logger() {
    return _LoggerErrorHandler();
  }

  void call(object, {bool test(Object error)}) {
    onError(object);
  }

  void onError(dynamic exception);
}

class _LoggerErrorHandler extends ErrorHandler {
  @override
  void onError(dynamic exception) {
    if (exception is BluetoothError) {
      print(exception.message);
    } else if (exception is Error) {
      print(exception.stackTrace);
    } else if (exception is Exception) {
      print(exception.toString());
    } else {
      print(exception.message);
    }
  }
}
