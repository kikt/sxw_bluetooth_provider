import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:sxw_bluetooth_provider/src/device/platform_enum.dart';

import '../../config/config.dart';

class PrinterBleConnectConfig extends BleConfig<Uint8List> {
  PrinterBleConnectConfig({
    @required String name,
  })  : assert(name != null),
        super(name: name) {
    notifyServiceId = getServiceId();
    notifyChId = getNotifyChId();
    writeServiceId = getServiceId();
    writeChId = getWriteChId();
  }

  String getServiceId() {
    if (Platform.isAndroid) {
      return "000018f0-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "18f0";
    }

    return "";
  }

  String getNotifyChId() {
    if (Platform.isAndroid) {
      return "00002af0-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "2af0";
    }

    return "";
  }

  String getWriteChId() {
    if (Platform.isAndroid) {
      return "00002af1-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "2af1";
    }

    return "";
  }

  @override
  String get platformType => "打印机";

  @override
  PlatformEnum get platformTypeEnum => PlatformEnum.printer;
}
