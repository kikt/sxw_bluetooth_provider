import 'package:sxw_bluetooth_provider/src/config/config.dart';
import 'package:sxw_bluetooth_provider/src/device/platform_enum.dart';

class PrinterSppConfig extends SppConfig {
  PrinterSppConfig(
    String name,
    String pin, {
    Duration scanTimeout,
  }) : super(
          pin: pin,
          name: name,
          duration: scanTimeout,
        );

  @override
  String get platformType => "打印机";

  @override
  PlatformEnum get platformTypeEnum => PlatformEnum.printer;
}
