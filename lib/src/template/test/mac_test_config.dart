import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:sxw_bluetooth_provider/src/config/config.dart';
import 'package:sxw_bluetooth_provider/src/device/platform_enum.dart';

class MacTestConnectConfig extends BleConfig {
  MacTestConnectConfig({
    @required String name,
  })  : assert(name != null),
        super(name: name) {
    notifyServiceId = getServiceId();
    notifyChId = getNotifyChId();
    writeServiceId = getServiceId();
    writeChId = getWriteChId();
  }

  String getServiceId() {
    if (Platform.isAndroid) {
      return "000018110000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "1811";
    }

    return "";
  }

  String getNotifyChId() {
    if (Platform.isAndroid) {
      return "00001818-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "1818";
    }

    return "";
  }

  String getWriteChId() {
    if (Platform.isAndroid) {
      return "00001818-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "1818";
    }

    return "";
  }

  @override
  String get platformType => "mac模拟外设";

  @override
  PlatformEnum get platformTypeEnum => PlatformEnum.printer;
}
