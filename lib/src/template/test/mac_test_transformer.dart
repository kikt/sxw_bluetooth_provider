import 'dart:typed_data';

import 'package:sxw_bluetooth_provider/src/device/bluetooth_device.dart';

class MacTestTransformer extends BtTransformer<Uint8List, Uint8List> {
  @override
  Uint8List convert(Uint8List value) {
    return value;
  }
}
