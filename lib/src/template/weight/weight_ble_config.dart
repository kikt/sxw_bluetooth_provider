import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/foundation.dart';
import 'package:sxw_bluetooth_provider/src/device/platform_enum.dart';

import '../../config/config.dart';

class WeightBleConnectConfig extends BleConfig<Uint8List> {
  WeightBleConnectConfig({
    @required String name,
  })  : assert(name != null),
        super(name: name) {
    notifyServiceId = getServiceId();
    notifyChId = getChId();
    writeServiceId = getServiceId();
    writeChId = getChId();
  }

  String getServiceId() {
    if (Platform.isAndroid) {
      return "0000ffe0-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "FFE0";
    }

    return "";
  }

  String getChId() {
    if (Platform.isAndroid) {
      return "0000ffe1-0000-1000-8000-00805f9b34fb";
    } else if (Platform.isIOS) {
      return "FFE1";
    }

    return "";
  }

  @override
  String get platformType => "电子秤";

  @override
  PlatformEnum get platformTypeEnum => PlatformEnum.weight;
}
