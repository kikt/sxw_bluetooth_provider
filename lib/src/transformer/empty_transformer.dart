import 'dart:typed_data';

import 'package:sxw_bluetooth_provider/src/device/bluetooth_device.dart';

class EmptyTransformer extends BtTransformer<Uint8List, Uint8List> {
  const EmptyTransformer();

  Uint8List call(Uint8List data) {
    return data;
  }

  @override
  convert(value) {
    return value;
  }
}
