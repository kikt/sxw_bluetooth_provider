import 'dart:async';

class NoHupCompleter<T> with Completer<T> {
  final _delegate = Completer<T>();

  @override
  void complete([FutureOr value]) {
    if (!_delegate.isCompleted) {
      _delegate.complete(value);
    }
  }

  @override
  void completeError(Object error, [StackTrace stackTrace]) {
    if (!_delegate.isCompleted) {
      _delegate.completeError(error, stackTrace);
    }
  }

  @override
  bool get isCompleted => _delegate.isCompleted;

  @override
  Future<T> get future => _delegate.future;
}
